import java.sql.*;
import java.util.LinkedList;

public class Main {
    // JDBC driver name and database URL
    static final String DB_URL = "jdbc:postgresql://localhost:5432/company";

    //  Database credentials
    static final String USER = "username";
    static final String PASS = "password";

    public static void main(String[] args) {
        LinkedList<Integer> ids = new LinkedList<>();
        ids.add(200);
        ids.add(201);
        ids.add(202);

        LinkedList<Integer> employeeIds = new LinkedList<>();
        employeeIds.add(106);
        employeeIds.add(107);
        employeeIds.add(108);

        LinkedList<Integer> amount = new LinkedList<>();
        amount.add(100);
        amount.add(200);
        amount.add(300);

        addSalaries(ids, employeeIds, amount);
    }

    public static void printRs(ResultSet rs) throws SQLException{
        //Ensure we start with first row
        rs.beforeFirst();
        while(rs.next()){
            //Retrieve by column name
            int id  = rs.getInt("id");
            int age = rs.getInt("age");
            String first = rs.getString("first");
            String last = rs.getString("last");

            //Display values
            System.out.print("ID: " + id);
            System.out.print(", Age: " + age);
            System.out.print(", First: " + first);
            System.out.println(", Last: " + last);
        }
        System.out.println();
    }

    public static void addSalaries(LinkedList<Integer> ids, LinkedList<Integer> employeeIds, LinkedList<Integer> amount){
        Connection conn = null;
        Statement stmt = null;
        try{
            Class.forName("org.postgresql.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);

            System.out.println("Creating statement...");
            stmt = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);

            String SQL = "";

            for (int i = 0; i < Math.max(ids.size(), Math.max(employeeIds.size(), amount.size())); i++) {
                System.out.println("Inserting one row....");
                SQL = "INSERT INTO public.salary " +
                        "VALUES (" + ids.get(i) + "," +
                        employeeIds.get(i) + "," +
                        amount.get(i)+ ")";

                stmt.executeUpdate(SQL);
            }

            System.out.println("Commiting data here....");
            conn.commit();

            stmt.close();
            conn.close();
        }catch(SQLException | IndexOutOfBoundsException se){
            
            se.printStackTrace();
            
            System.out.println("Rolling back data here....");
            try{
                if(conn!=null)
                    conn.rollback();
            }catch(SQLException se2){
                se2.printStackTrace();
            }

        }catch(Exception e){
            
            e.printStackTrace();
        }finally{
            
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        System.out.println("Goodbye!");

    }
}
